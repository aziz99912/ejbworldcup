package useruser.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Stade implements Serializable {
 @GeneratedValue(strategy = GenerationType.IDENTITY)
 @Id
    private int id ; 
	private String nom ;
	private String Localisation ;
	private Number Capacity ;
	
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getLocalisation() {
		return Localisation;
	}
	public void setLocalisation(String localisation) {
		Localisation = localisation;
	}
	public Number getCapacity() {
		return Capacity;
	}
	public void setCapacity(Number capacity) {
		Capacity = capacity;
	}
	@Override
	public String toString() {
		return "Stade [id=" + id + ", nom=" + nom + ", Localisation=" + Localisation + ", Capacity=" + Capacity + "]";
	}
	
	
	
	
	

}
