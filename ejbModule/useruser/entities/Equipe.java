package useruser.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Equipe implements Serializable  {
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id 
	  private int id ; 
	private String nom ; 
	private String joueur ; 
	private String entraineur ;
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getJoueur() {
		return joueur;
	}
	public void setJoueur(String joueur) {
		this.joueur = joueur;
	}
	public String getEntraineur() {
		return entraineur;
	}
	public void setEntraineur(String entraineur) {
		this.entraineur = entraineur;
	}
	@Override
	public String toString() {
		return "Equipe [nom=" + nom + ", joueur=" + joueur + ", entraineur=" + entraineur + "]";
	} 
	
	

}
