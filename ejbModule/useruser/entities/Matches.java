package useruser.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
@Entity
public class Matches  implements Serializable  {
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	
	@Id
	
	private String equipe ; 
	private Date datematch ;
	private String stadess;
	
	public Matches() {};
	
	public Matches(String equipe, Date datematch, String stadess) {
		super();
		this.equipe = equipe;
		this.datematch = datematch;
		this.stadess = stadess;
	}



	public String getEquipe() {
		return equipe;
	}



	public void setEquipe(String equipe) {
		this.equipe = equipe;
	}



	public Date getDatematch() {
		return datematch;
	}



	public void setDatematch(Date datematch) {
		this.datematch = datematch;
	}



	public String getStadess() {
		return stadess;
	}



	public void setStadess(String stadess) {
		this.stadess = stadess;
	}



	@Override
	public String toString() {
		return "Matches [equipe=" + equipe + ", datematch=" + datematch + ", stadess=" + stadess + "]";
	}
	
	
	
	

}
