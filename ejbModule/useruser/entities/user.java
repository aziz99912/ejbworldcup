package useruser.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
@Entity
public class user  implements Serializable{
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	 
	private String prenom ; 
	private String nom ;    
	private String email; 
	private String telephone ;
	private String password ; 
	
	
	
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	@Override
	public String toString() {
		return "user [prenom=" + prenom + ", nom=" + nom + ", email=" + email + ", telephone=" + telephone
				+ ", password=" + password + "]";
	}
	
	
	
}
