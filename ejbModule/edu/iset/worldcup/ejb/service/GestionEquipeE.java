package edu.iset.worldcup.ejb.service;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import useruser.entities.Equipe;

/**
 * Session Bean implementation class GestionEquipeE
 */
@Stateless
@LocalBean
public class GestionEquipeE implements GestionEquipeERemote, GestionEquipeELocal {
	@PersistenceContext
	private EntityManager entitymanager ;
	
	

	
	public void addequipe(Equipe equipe) {
		entitymanager.persist(equipe);
		
	}

	
	public void updateequipe(Equipe equipe) {
		entitymanager.merge(equipe);
		
		
	}

	
	public Equipe findequipeID(int i) {
		Equipe e=entitymanager.find(Equipe.class,i);
		return e ;	
	}

	
	public void deleteequipe(Equipe equipe) {
		entitymanager.remove(entitymanager.merge(equipe));
		
		
	}

	
	public List<Equipe> findAllequipe() {
		Query q = entitymanager.createQuery("From equipe e" , Equipe.class);
		return (q.getResultList());
	}

}
