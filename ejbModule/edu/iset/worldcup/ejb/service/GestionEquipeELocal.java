package edu.iset.worldcup.ejb.service;

import java.util.List;

import javax.ejb.Local;

import useruser.entities.Equipe;

@Local
public interface GestionEquipeELocal {
	public void addequipe (Equipe equipe) ; 
	 public void updateequipe(Equipe equipe )  ; 
	 public Equipe findequipeID(int i) ; 
	 public void deleteequipe(Equipe equipe) ; 
	 public List<Equipe> findAllequipe() ; 


}
