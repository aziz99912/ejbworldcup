package edu.iset.worldcup.ejb.services;

import java.util.List;

import javax.ejb.Local;

import useruser.entities.Stade;

@Local
public interface GestionStadeWLocal {
	 public void addstade (Stade stade) ; 
	 public void updatestade (Stade stade)  ; 
	 public Stade findstadeID(int i) ; 
	 public void deletestade(Stade stade) ; 
	 public List<Stade> findAllstade() ; 

}
