package edu.iset.worldcup.ejb.services.match;

import java.util.List;

import javax.ejb.Remote;

import useruser.entities.Matches;


@Remote
public interface GestionmatchRemote {
	 public void addmatch (Matches Match) ; 
	 public void updatematch (Matches Match)  ; 
	 public Matches findmatchID(int i) ; 
	 public void deletematch(Matches Match) ; 
	 public List<Matches> findAllmatch() ; 

}
