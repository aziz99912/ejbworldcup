package edu.iset.worldcup.ejb.services.match;

import java.util.List;

import javax.ejb.Local;

import useruser.entities.Matches;


@Local
public interface GestionmatchLocal {
	 public void addmatch (Matches Match) ; 
	 public void updatematch (Matches Match)  ; 
	 public Matches findmatchID(int i) ; 
	 public void deletematch(Matches Match) ; 
	 public List<Matches> findAllmatch() ; 

}
