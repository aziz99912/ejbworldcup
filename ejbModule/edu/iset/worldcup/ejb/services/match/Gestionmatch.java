package edu.iset.worldcup.ejb.services.match;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import useruser.entities.Matches;


/**
 * Session Bean implementation class GestionStadeW
 */
@Stateless
public class Gestionmatch implements GestionmatchRemote, GestionmatchLocal {

	@PersistenceContext
	private EntityManager entitymanager;

	public void addmatch(Matches Match) {
		entitymanager.persist(Match);

	}

	public void updatematch(Matches Match) {
		entitymanager.merge(Match);

	}

	public Matches findmatchID(int i) {
		Matches s = entitymanager.find(Matches.class, i);

		return s;
	}

	public void deletematch(Matches Match) {
		entitymanager.remove(entitymanager.merge(Match));

	}

	public List<Matches> findAllmatch() {
		Query q = entitymanager.createQuery("From stade s", Matches.class);
		return (q.getResultList());

	}

}
