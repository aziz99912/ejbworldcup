package edu.iset.worldcup.ejb.services;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import useruser.entities.Stade;

/**
 * Session Bean implementation class GestionStadeW
 */
@Stateless
public class GestionStadeW implements GestionStadeWRemote, GestionStadeWLocal {

	@PersistenceContext
	private EntityManager entitymanager;

	public void addstade(Stade stade) {
		entitymanager.persist(stade);

	}

	public void updatestade(Stade stade) {
		entitymanager.merge(stade);

	}

	public Stade findstadeID(int i) {
		Stade s = entitymanager.find(Stade.class, i);

		return s;
	}

	public void deletestade(Stade stade) {
		entitymanager.remove(entitymanager.merge(stade));

	}

	public List<Stade> findAllstade() {
		Query q = entitymanager.createQuery("From stade s", Stade.class);
		return (q.getResultList());

	}

}
