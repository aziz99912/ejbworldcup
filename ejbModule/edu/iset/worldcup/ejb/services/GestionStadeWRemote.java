package edu.iset.worldcup.ejb.services;

import java.util.List;

import javax.ejb.Remote;

import useruser.entities.Stade;

@Remote
public interface GestionStadeWRemote {
	 public void addstade (Stade stade) ; 
	 public void updatestade (Stade stade)  ; 
	 public Stade findstadeID(int i) ; 
	 public void deletestade(Stade stade) ; 
	 public List<Stade> findAllstade() ; 

}
