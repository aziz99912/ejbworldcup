package edu.iset.worldcup.ejb.servicees;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import useruser.entities.user;

/**
 * Session Bean implementation class GestionuserU
 */
@Stateless
@LocalBean
public class GestionuserU implements GestionuserURemote, GestionuserULocal {
	@PersistenceContext
	private EntityManager entitymanager;
	public user login(String email, String password) {
		Query query ;
		try {
			String myFirstJPQLQuery="SELECT u FROM userU WHERE u.email = :p1 AND u.password= :p2";
			query = entitymanager.createQuery(myFirstJPQLQuery);
			query.setParameter("p1", email);
			query.setParameter("p2", password);
			return (user) query.getSingleResult();
		}
		catch (NoResultException e) {
			e.printStackTrace();
			return null;
		}
	}

	public void adduser(user user) {
		entitymanager.persist(user);
	}

	public void updateuser(user user) {
		entitymanager.merge(user);
	}
	public user finduserID(int i) {
		user u = entitymanager.find(user.class, i);
		return u;
	}

	public void deleteuser(user user) {
		entitymanager.remove(entitymanager.merge(user));
	}

	public List<user> findAlluseres() {
		Query q = entitymanager.createQuery("From user u", user.class);
		return (q.getResultList());
	}

}
