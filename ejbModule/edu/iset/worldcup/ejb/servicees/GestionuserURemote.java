package edu.iset.worldcup.ejb.servicees;

import java.util.List;

import javax.ejb.Remote;

import useruser.entities.user;

@Remote
public interface GestionuserURemote {
	public user login(String email, String password);
	public void adduser (user user) ; 
	 public void updateuser(user user)  ; 
	 public user finduserID(int i) ; 
	 public void deleteuser(user user) ; 
	 public List<user> findAlluseres() ; 

}
